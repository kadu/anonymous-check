<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl_PL">
<context>
    <name>AnonCheck::AnonymousCheck</name>
    <message>
        <location filename="../anonymous_check.cpp" line="100"/>
        <source>Female</source>
        <translation>Kobieta</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="102"/>
        <source>Male</source>
        <translation>Mężczyzna</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="105"/>
        <source>Contact Info</source>
        <translation>Dane Kontaktu</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="108"/>
        <source>First Name</source>
        <translation>Imię</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="109"/>
        <source>Last Name</source>
        <translation>Nazwisko</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="110"/>
        <source>Family Name</source>
        <translation>Nazwisko Rodowe</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="111"/>
        <source>City</source>
        <translation>Miasto</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="112"/>
        <source>Family City</source>
        <translation>Miasto Rodzinne</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="113"/>
        <source>Nick Name</source>
        <translation>Ksywa</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="114"/>
        <source>Home Phone</source>
        <translation>Tel</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="115"/>
        <source>Mobile Phone</source>
        <translation>Kom</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="116"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="117"/>
        <source>Website</source>
        <translation>Strona WWW</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="118"/>
        <source>Gender</source>
        <translation>Płeć</translation>
    </message>
    <message>
        <location filename="../anonymous_check.cpp" line="121"/>
        <source>Birth Year</source>
        <translation>Rok Urodzenia</translation>
    </message>
</context>
</TS>
